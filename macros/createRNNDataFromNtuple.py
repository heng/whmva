#!/usr/bin/env python

import bisect
import copy
import re
import gc
import logging
import os
import psutil
import sys
import socket
import time

import uproot
import h5py
import numpy as np

from optparse import OptionParser
p = OptionParser()

p.add_option('-o', '--outkey',   type='string',     default=None)
p.add_option('--tree-name',      type='string',     default='tree_prepCandMVAMuon')

p.add_option('--nevent', '-n',   type='int',   default=None)
p.add_option('--njet',           type='int',   default=5)
p.add_option('--nlep',           type='int',   default=2)
p.add_option('--nclass',         type='int',   default=2)
p.add_option('--photon-frac',    type='float', default=0.15)

p.add_option('-d', '--debug',    action='store_true',  default=False)
p.add_option('-e', '--explore',  action='store_true',  default=False)
p.add_option('--linear-weight',  action='store_true',  default=False)

(options,args) = p.parse_args()

#======================================================================================================        
def getOutKey():
    
    if not options.outkey:
        return None

    key  = options.outkey
    key += '_njet%d'    %options.njet
    key += '_nlepvar%d' %len(getLepTrainVars())
    key += '_njetvar%d' %len(getJetTrainVars())
    key += '_nclass%d'  %options.nclass

    if options.nevent:
        if options.nevent < 1000000:
            key += '_nevt%dk' %int(options.nevent/1000)
        else:
            key += '_nevt%dm' %int(options.nevent/1000000)

    return key

#======================================================================================================        
def getLog(name, level='INFO', debug=False, print_time=False):

    if print_time:
        f = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
    else:
        f = logging.Formatter('%(message)s')

    sh = logging.StreamHandler(sys.stdout)
    sh.setFormatter(f)
    
    okey = getOutKey()

    log = logging.getLogger(name)
    log.addHandler(sh)
    
    if okey:        
        fh = logging.FileHandler('%s.log' %okey)
        fh.setFormatter(f)
        log.addHandler(fh)

    if debug:
        log.setLevel(logging.DEBUG)
    else:
        if level == 'DEBUG':   log.setLevel(logging.DEBUG)
        if level == 'INFO':    log.setLevel(logging.INFO)
        if level == 'WARNING': log.setLevel(logging.WARNING)    
        if level == 'ERROR':   log.setLevel(logging.ERROR)

    return log

#======================================================================================================
def getInputVars():
    
    input_vars = ['Event',
                  'MCChannel',
                  'NJet',
                  'Mll',
                  'DRll',
                  'METFinalEMTopo_MET',
                  'METFinalEMTopo_Phi',
                  'RankMJetsLep1_MJetsLep',
                  'RankMJetsLep1_DRJetsLep',
                  'RankMJetsLep1_DPhiJetsLep',
                  'RankMJetsLep2_MJetsLep',
                  'RankMJetsLep2_DRJetsLep',
                  'RankMJetsLep2_DPhiJetsLep',
                  ]

    return sorted(input_vars + getLepTrainVars() + getJetTrainVars())

#======================================================================================================
def getLepTrainVars():
    
    input_vars = ['m_leps_Pt',
                  'm_leps_Eta',
                  'm_leps_Phi',
                  ]
    
    return sorted(input_vars)

#======================================================================================================
def getJetTrainVars():
    
    input_vars = ['m_jets_Pt',
                  'm_jets_Eta',
                  'm_jets_Phi',
                  ]
    
    return sorted(input_vars)

#======================================================================================================        
log = getLog(os.path.basename(__file__), print_time=False)

#======================================================================================================
class VarMan:

    '''Class to map between variable index in numpy array and variable/branch name in ntuple'''
    
    def __init__(self, input_vars, lep_train_vars, jet_train_vars):

        train_vars = lep_train_vars + jet_train_vars

        for v in train_vars:
            if v not in input_vars:
                raise Exception('VarMan - train var "%s" is not on input var list' %v)

        if len(input_vars) != len(set(input_vars)):
                raise Exception('VarMan - input vars are not unique: %s' %input_vars)

        if len(train_vars) != len(set(train_vars)):
                raise Exception('VarMan - train vars are not unique: %s' %train_vars)
            
        self.train_vars     = sorted(train_vars)
        self.lep_train_vars = sorted(lep_train_vars)
        self.jet_train_vars = sorted(jet_train_vars)
        self.extra_vars     = sorted([x for x in input_vars if x not in train_vars])
        self.input_vars     = self.train_vars + self.extra_vars

        self.bytes_read_event_attrs = ['']
        
        self.bytes_dict_train_vars     = {}
        self.bytes_dict_lep_train_vars = {}
        self.bytes_dict_jet_train_vars = {}
        self.bytes_dict_input_vars     = {}
        self.bytes_dict_extra_vars     = {}

        self.index_dict_input_vars     = {}

        for i in range(len(self.train_vars)):
            self.bytes_dict_train_vars[self.train_vars[i].encode()] = i

        for i in range(len(self.lep_train_vars)):
            self.bytes_dict_lep_train_vars[self.lep_train_vars[i].encode()] = i

        for i in range(len(self.jet_train_vars)):
            self.bytes_dict_jet_train_vars[self.jet_train_vars[i].encode()] = i

        for i in range(len(self.extra_vars)):
            self.bytes_dict_extra_vars[self.extra_vars[i].encode()] = i

        for i in range(len(self.input_vars)):
            self.bytes_dict_input_vars[self.input_vars[i].encode()] = i
            self.index_dict_input_vars[i] = self.input_vars[i].encode()             

        self.PrintVars()
            
    def VarToBytes(self, varname):
        if type(varname) == str:
            return varname.encode()
        elif type(varname) == bytes:
            return varname
        elif type(varname) == int:
            return self.index_dict_input_vars[i]
        
        raise Exception('VarMan::VarToBytes - unknown variable type: %s' %varname)

    def GetTrainVars(self):
        return self.train_vars        

    def GetLepTrainVars(self):
        return self.lep_train_vars        
    
    def GetJetTrainVars(self):
        return self.jet_train_vars        

    def GetExtraVars(self):
        return self.extra_vars

    def GetNTrainVars(self):
        return len(self.train_vars)

    def GetNLepTrainVars(self):
        return len(self.lep_train_vars)

    def GetNJetTrainVars(self):
        return len(self.jet_train_vars)

    def GetNExtraVars(self):
        return len(self.extra_vars)
    
    def IsTrainVar(self, varname):        
        return self.VarToBytes(varname) in self.bytes_dict_train_vars

    def GetVarIndex(self, varname):
        return self.bytes_dict_input_vars[self.VarToBytes(varname)]

    def GetLepVarIndex(self, varname):
        return self.bytes_dict_lep_train_vars[self.VarToBytes(varname)]

    def GetJetVarIndex(self, varname):
        return self.bytes_dict_jet_train_vars[self.VarToBytes(varname)]

    def GetExtraVarIndex(self, varname):
        return self.bytes_dict_extra_vars[self.VarToBytes(varname)]

    def PrintVars(self):
        log.info('VarMan - %d input vars: %s' %(len(self.input_vars), self.input_vars))
        log.info('VarMan - %d train vars: %s' %(len(self.train_vars), self.train_vars))
        log.info('VarMan - %d extra vars: %s' %(len(self.extra_vars), self.extra_vars))

        for v, i in self.bytes_dict_input_vars.items():
            log.info('   index %2d - %s' %(i, v.decode())) 
        
#======================================================================================================
varMan = VarMan(getInputVars(), getLepTrainVars(), getJetTrainVars())    
        
#======================================================================================================
class Event:

    event_bvar = ('Event'    ).encode()
    njet_bvar  = ('NJet'     ).encode()
    truth_mc   = ('MCChannel').encode()
    
    njet_range    = range(options.njet)

    def __init__(self, data, index):

        '''Class to hold event data and provide useful functions'''
        
        self.index          = index
        self.lep_train_data = np.zeros((options.nlep, varMan.GetNLepTrainVars()))
        self.jet_train_data = np.zeros((options.njet, varMan.GetNJetTrainVars()))
        self.extra_data     = np.zeros(varMan.GetNExtraVars())
        self.input_njet     = None
        
        for key, value in data.items():        
            curr_value  = value[index]
            
            if varMan.IsTrainVar(key):
                #
                # Save training variable to numpy array
                # 
                if ('%s'%key).count('m_leps'):
                    for ilep in range(options.nlep):
                        if ilep < len(curr_value):
                            log.debug('%s VarIndex=%d, ilep=%d' %(key, varMan.GetLepVarIndex(key), ilep))
                            self.lep_train_data[ilep, varMan.GetLepVarIndex(key)] = curr_value[ilep]

                elif ('%s'%key).count('m_jets'): 
                    for ijet in Event.njet_range:
                        if ijet < len(curr_value):
                            log.debug('%s VarIndex=%d, ijet=%d' %(key, varMan.GetJetVarIndex(key), ijet))
                            self.jet_train_data[ijet, varMan.GetJetVarIndex(key)] = curr_value[ijet]
            else:
                #
                # Save extra observer variables
                #
                self.extra_data[varMan.GetExtraVarIndex(key)] = curr_value

        self.Event              = data[Event.event_bvar  ][index]
        self.njet               = data[Event.njet_bvar   ][index]  
        self.MCChannel          = data[Event.truth_mc    ][index]  
        self.event_label        = self.ComputeEventLabel()
        self.event_weight       = None
        
        self.SetEventWeight(1)

    def ClearEvent(self):
        self.lep_train_data = None
        self.jet_train_data = None
        self.extra_data = None

    def GetLepTrainData(self):
        return self.lep_train_data

    def GetJetTrainData(self):
        return self.jet_train_data
    
    def GetExtraData(self):
        return self.extra_data

    def GetEventIndex(self):
        return self.index
    
    def GetEventWeight(self):
        if self.event_weight == None:
            raise Exception('Event::GetEventWeight - weight is not set')
        return self.event_weight
            
    def SetEventWeight(self, weight):
        if self.event_weight != None:
            raise Exception('Event::SetEventWeight - weight is already set')
        self.event_weight = weight

    def GetEventLabel(self):
        return self.event_label

    def IsTrainEvent(self):
        return (self.Event % 2) == 0

    def IsTestEvent(self):
        return not self.IsTrainEvent()

    def ComputeEventLabel(self):

        if options.nclass == 2:
            if self.MCChannel in [363357, 363489, 363358, 364253, 364289, 364284]:
                # background, WZ
                return 0
                                                                    
            elif self.MCChannel in [346644, 346643]: 
                # signal, WH ss 2lepton qq
                return 1
            
            else:
                return -99

        else:
            raise Exception('Event::ComputeEventLabel - unknown option --nclass=%s' %options.nclass)

        return -1

    def EventAsStr(self):
        s  = 'Event index=%d' %self.GetEventIndex()
        s += ', label=%d'     %self.GetEventLabel()
        s += ', is_train=%d'  %self.IsTrainEvent()
        s += ', NJet=%.1f'    %self.njet

        return s

#======================================================================================================        
# Functions
#======================================================================================================        
def getClassLabelNames():
    
    '''This order MUST match class label definitions of Event::ComputeEventLabel function above'''

    if options.nclass == 2:
        return ['WH', 'WZ']

    raise Exception('getClassLabelNames - unknown option --nclass=%s' %options.nclass)

#======================================================================================================        
def printMem(key):
    
    process = psutil.Process(os.getpid())
    rss = process.memory_info().rss/pow(10.0, 6)

    log.info('%s%.1f MB' %(key, rss))

#======================================================================================================        
def printOpts():
    
    log.info('  --nevent=%s'       %options.nevent)
    log.info('  --nclass=%d'       %options.nclass)
    log.info('  --njet=%d'         %options.njet)
    log.info('    outkey=%s'       %getOutKey())

#======================================================================================================        
def exploreFile(fpath, branches):
    
    if not os.path.isfile(fpath):
        log.warning('exploreFile - invalid file: %s' %fpath)
        return 

    fdata = uproot.open(fpath)

    if options.tree_name.encode() not in fdata:
        log.warning('exploreFile - file: %s is missing tree "%s"' %(fpath,options.tree_name ))
        return 

    data = fdata[options.tree_name.encode()]

    log.info('Print data: %s' %data)
    log.info('Print dir(data): %s' %dir(data))
    log.info('Print keys: %s' %data.keys())
    data.show()

    icount = 0

    branches = getLepTrainVars() + getInputVars() + getJetTrainVars()
    
    for dvals in data.iterate(branches, entrystart=0, entrystop=options.nevent, entrysteps=None):
        icount += 1
        
        for key, data in dvals.items():    
            log.info('key=%s, len=%d, %s' %(key, len(data), data))

    log.info('icount=%d' %icount)

#======================================================================================================        
def readFileData(fpath, branches):
    
    if not os.path.isfile(fpath):
        log.warning('readFile - invalid file: %s' %fpath)
        return 

    fdata = uproot.open(fpath)

    if options.tree_name.encode() not in fdata:
        log.warning('readFileData - file: %s is missing tree "%s"' %(fpath,options.tree_name ))
        return 

    utree = fdata[options.tree_name.encode()]

    icount = 0
    train_events = []
    test_events  = []

    count_train_labels = {}
    count_test_labels  = {}

    timePrev = time.time()

    for evt_data in utree.iterate(branches=branches, entrystop=options.nevent):
        len_data = 0
        
        for key, data in evt_data.items():
            
            branch = key.decode('utf-8')
            
            if len_data == 0:
                len_data = len(data)
                log.info('   read %5d branch entries' %(len_data))
            
            elif len_data != len(data):
                raise Exception('readFile - logic error: branch "%s" have different length: %d != %d' %(branch, len_data, len(data)))

        for i in range(len_data):
            icount += 1
            
            if icount % 20000 == 0:
                log.info('Processing event #%7d, delta t=%.2fs' %(icount, time.time() - timePrev))
                timePrev = time.time()

            event = Event(evt_data, i)

            if event.IsTrainEvent(): 
                try:
                    count_train_labels[event.GetEventLabel()] += 1
                except KeyError:
                    count_train_labels[event.GetEventLabel()]  = 1
            elif event.IsTestEvent():
                try:
                    count_test_labels[event.GetEventLabel()] += 1
                except KeyError:
                    count_test_labels[event.GetEventLabel()]  = 1
                
            if event.GetEventLabel() >= 0:        
                if event.IsTrainEvent(): 
                    train_events += [event]
                elif event.IsTestEvent():  
                    test_events += [event]

            if options.nevent and icount >= options.nevent:
                break            

    log.info('Number of total training events: %d' %len(train_events))

    for iclass, counts in count_train_labels.items():
        log.info('   class=%2d number of training events = %d' %(iclass, counts))


    log.info('Number of total testing events: %d' %len(test_events))

    for iclass, counts in count_test_labels.items():
        log.info('   class=%2d number of testing events = %d' %(iclass, counts))

    log.info('readFile - all is done for %s' %fpath)

    return (train_events, test_events)

#======================================================================================================        
def prepNumpyArrays(key, events, result):

    '''Prepare training and test numpy arrays'''

    printMem('prepNumpyArrays - %s: begin ' %key)

    gc.collect()

    printMem('prepNumpyArrays - %s: begin after garbage collection ' %key)
    
    #
    # Create training arrays
    #
    out_lep_train_data = np.zeros((len(events), options.nlep, varMan.GetNLepTrainVars()))
    out_jet_train_data = np.zeros((len(events), options.njet, varMan.GetNJetTrainVars()))
    out_extra_data = np.zeros((len(events), varMan.GetNExtraVars()))

    out_labels  = np.zeros( len(events), dtype=np.int8)
    out_weights = np.zeros( len(events), dtype=np.float32)

    printMem('prepNumpyArrays - %s: allocated numpy arrays for training events ' %key)
    
    for i in range(len(events)):
        event = events[i]
        
        out_lep_train_data[i] = event.GetLepTrainData()
        out_jet_train_data[i] = event.GetJetTrainData()
        out_extra_data[i] = event.GetExtraData()

        out_labels[i]  = event.GetEventLabel()
        out_weights[i] = event.GetEventWeight()

        if options.debug:
            print(event.EventAsStr())
            print(event.GetLepTrainData())
            print(event.GetJetTrainData())
            print(event.GetExtraData())
            print(out_extra_data[i]) 

        event.ClearEvent()


    printMem('prepNumpyArrays - %s: filled arrays for training events '%key) 

    events = []
    gc.collect()

    printMem('prepNumpyArrays - %s: deleted input events ' %key)
        
    log.info('Out lep train data size=%d, shape=%s' %(out_lep_train_data.size, out_lep_train_data.shape))
    log.info('Out jet train data size=%d, shape=%s' %(out_jet_train_data.size, out_jet_train_data.shape))
    log.info('Out extra data size=%d, shape=%s' %(out_extra_data.size, out_extra_data.shape))
    
    for iclass in range(options.nclass):
        select_out_labels = out_labels == iclass

        log.info('   %s class %d: number events = %d' %(key, iclass, np.sum(select_out_labels)))

    #
    # Put results into dictionary
    #
    result['%s_lep_data' %key]   = out_lep_train_data
    result['%s_jet_data' %key]   = out_jet_train_data
    result['%s_extra_data' %key] = out_extra_data
    result['%s_labels'     %key] = out_labels
    result['%s_weights'    %key] = out_weights

    printMem('prepNumpyArrays - %s: all done ' %key)

    return result
    
#======================================================================================================        
def main():

    if len(args) != 1:
        log.warning('Wrong number of command line arguments: %s' %len(args))
        return

    if options.explore:
        return exploreFile(args[0], getInputVars())        
    
    train_events, test_events = readFileData(args[0], getInputVars())

    log.info('main - prepared training events: %d' %len(train_events))
    log.info('main - prepared testing  events: %d' %len(test_events))
    
    if not getOutKey():
        log.info('main - output file is not set so nothing more to do')
        return

    #
    # Save training and testing data, and names of training variables saved at matching array index
    #
    outname = getOutKey() + '.h5'

    result = {}
    
    prepNumpyArrays('train', train_events, result)
    prepNumpyArrays('test',  test_events,  result)
        
    log.info('main - saving results to: %s' %outname)
        
    ofile = h5py.File(outname, 'w')

    for key, value in result.items():
        log.info('Save %-20s with data shape=%-20s, size=%d' %('"%s"' %key, str(value.shape), len(value)))

        ofile.create_dataset(key, data=value)

    #
    # Record variable and class names
    #
    lep_train_vars = varMan.GetLepTrainVars()
    jet_train_vars = varMan.GetJetTrainVars()
    extra_vars     = varMan.GetExtraVars()
    class_labels   = getClassLabelNames()
    
    lep_train_var_set = ofile.create_dataset('lep_train_vars',   (len(lep_train_vars),),   dtype=h5py.special_dtype(vlen=str))
    jet_train_var_set = ofile.create_dataset('jet_train_vars',   (len(jet_train_vars),),   dtype=h5py.special_dtype(vlen=str))
    extra_var_set = ofile.create_dataset('extra_vars',   (len(extra_vars),),   dtype=h5py.special_dtype(vlen=str))
    class_lab_set = ofile.create_dataset('class_labels', (len(class_labels),), dtype=h5py.special_dtype(vlen=str))

    for i in range(len(lep_train_vars)):
        lep_train_var_set[i] = lep_train_vars[i]

    for i in range(len(jet_train_vars)):
        jet_train_var_set[i] = jet_train_vars[i]

    for i in range(len(extra_vars)):
        extra_var_set[i] = extra_vars[i]

    log.info('Train %d vars: %s' %(len(lep_train_vars), ', '.join(lep_train_vars)))
    log.info('Train %d vars: %s' %(len(jet_train_vars), ', '.join(jet_train_vars)))
    log.info('Extra %d vars: %s' %(len(extra_vars), ', '.join(extra_vars)))
    
    for i in range(len(class_labels)):
        class_lab_set[i] = class_labels[i]
        
        log.info('class %d label="%s"' %(i, class_labels[i]))
            
#======================================================================================================        
if __name__ == '__main__':

    timeStart = time.time()

    printOpts()

    log.info('Start job at %s:%s' %(socket.gethostname(), os.getcwd()))
    log.info('Current time: %s' %(time.asctime(time.localtime())))

    main()

    printOpts()

    log.info('Local time: %s' %(time.asctime(time.localtime())))
    log.info('Total time: %.1fs' %(time.time()-timeStart))
