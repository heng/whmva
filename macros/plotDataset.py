#!/usr/bin/env python

import os
import socket
import sys
import time
import logging

import h5py
import numpy as np
import pandas as pd
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt

from joblib import Parallel,delayed

from optparse import OptionParser
p = OptionParser()

p.add_option('-n', '--nevent',   type='int',                         default=None)
p.add_option('-o', '--outdir',   type='string',                      default='plotInput')
p.add_option('-d', '--debug',    action='store_true',  dest='debug', default=False)

p.add_option('--do-rnn',         action='store_true',  default=False)
p.add_option('--do-rotation',    action='store_true',  default=False)

(options,args) = p.parse_args()

#======================================================================================================        
def getLog(name, level='INFO', debug=False, print_time=False):

    if print_time:
        f = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
    else:
        f = logging.Formatter('%(message)s')
        
    sh = logging.StreamHandler(sys.stdout)
    sh.setFormatter(f)
    
    log = logging.getLogger(name)
    log.addHandler(sh)
    
    if debug:
        log.setLevel(logging.DEBUG)
    else:
        if level == 'DEBUG':   log.setLevel(logging.DEBUG)
        if level == 'INFO':    log.setLevel(logging.INFO)
        if level == 'WARNING': log.setLevel(logging.WARNING)    
        if level == 'ERROR':   log.setLevel(logging.ERROR)

    return log

#======================================================================================================        
log = getLog(os.path.basename(__file__), print_time=False)
    
#======================================================================================================
def getOutName(name):
    
    if not options.outdir:
        return None
        
    outdir = '%s' %(options.outdir.rstrip('/'))
        
    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    return '%s/%s' %(outdir.rstrip('/'), name)

#======================================================================================================        
def RotationData(data, ref_obj):
    # Eta 
    if ref_obj[0] < 0:
        data[:, 0] = - data[:, 0]

    # Phi
    phi = ref_obj[1] 
    data[:, 1] = data[:, 1] - phi

    Pi = 3.1416
    data[:, 1][data[:, 1] < -Pi] = data[:, 1][data[:, 1] < -Pi] + 2*Pi
    data[:, 1][data[:, 1] >  Pi] = data[:, 1][data[:, 1] >  Pi] - 2*Pi

    return data
 
#======================================================================================================        
def GetMETVector(extra_data, extra_vars):
    ''' prepare the extra MET variables '''

    if extra_data.shape[1] != len(extra_vars):
        raise Exception('Wrong dimension for the Extra data and Extra vars')

    extra_data_df = pd.DataFrame(extra_data, columns = extra_vars)

    extra_data_df['METFinalEMTopo_Eta'] = 0

    #
    # save to (NEvents, NObj, NObjVars) numpy array
    # For the MET, NObj = 1
    #
    met_vector = extra_data_df[['METFinalEMTopo_Eta', 'METFinalEMTopo_Phi', 'METFinalEMTopo_MET']].values.reshape(-1, 1, 3) 

    return met_vector


#====================================================================================================== 
def plotVarData(var_name, var_index, data, labels, title, name=None):

    '''Plot data for selected variable'''

    log.info('PlotVarData - plot "%s" at index=%d, title=%s' %(var_name, var_index, title))
    
    label_name_dict = {0:'WZ', 1:'WH'}
    label_col_dict  = {0:'blue', 1:'red'}

    var_min = data.min()
    var_max = data.max()

    #
    # Plot 1d histogram with variable values
    #
    plt.clf()
    
    for i in label_name_dict.keys():
        select_data = data[labels == i]
        phd = plt.hist(select_data, range=(var_min, var_max), bins=100, histtype='step', color=label_col_dict[i], label=label_name_dict[i], density=True)

    plt.legend(loc='upper right')
    plt.xlabel(var_name)
    plt.ylabel('Density')
    plt.title(title)
    plt.grid(True)

    if name:
        fname = name
    else:
        fname = title

    plt.savefig('%s_%s.pdf' %(var_name, fname), format='pdf')
 
#====================================================================================================== 
def plotRNNVarData(var_name, var_index, sig_data, bkg_data, title='inputs', name=None):

    '''Plot data for selected variable'''

    log.info('PlotVarData - plot "%s" at index=%d' %(var_name, var_index))
    
    label_name_dict = {0:'WZ', 1:'WH'}
    label_col_dict  = {0:'blue', 1:'red'}

    data = [bkg_data, sig_data]

    var_min = min(sig_data.min(), bkg_data.min())
    var_max = max(sig_data.max(), bkg_data.min())
    
    #
    # Plot 1d histogram with variable values
    #
    plt.clf()
    
    for i in label_name_dict.keys():
        select_data = data[i]
        phd = plt.hist(select_data, range=(var_min, var_max), bins=100, histtype='step', color=label_col_dict[i], label=label_name_dict[i], density=True)

    plt.legend(loc='upper right')
    plt.xlabel(var_name)
    plt.ylabel('Density')
    plt.grid(True)

    if name:
        fname = name
    else:
        fname = title

    plt.savefig('%s.pdf' %getOutName('%s_%s'%(var_name, fname)), format='pdf')
    
#======================================================================================================        
def main():

    if len(args) != 1:
        log.warning('Wrong number of command line arguments: %s' %len(args))
        return

    fname = args[0]

    if not os.path.isfile(fname):
        log.warning('Input file does not exist: %s' %fname)
        return    

    ifile = h5py.File(fname, 'r')

    log.info('%s contains %d dataset keys' %(fname, len(ifile.keys())))

    maxl = len(max(ifile.keys(), key=len))
    
    for k in ifile.keys():
        d = ifile[k]        
        log.info('   %s len=%6d, size=%6d, dtype=%8s, shape=%s' %(('%s' %k).ljust(maxl), len(d), d.size, d.dtype, d.shape))

    train_vars = ifile['train_vars'][:]

    train_labels = ifile['train_labels'][:]    
    test_labels  = ifile['test_labels'][:]

    train_data  = ifile['train_data'][:]
    test_data   = ifile['test_data'][:]

    if len(test_data) != len(test_labels):
        raise Exception('Length of test data and labels do not match')

    if len(train_data) != len(train_labels):
        raise Exception('Length of test data and labels do not match')
    
    log.info('Make plots...')
        
    for i in range(len(train_vars)):
        plotVarData(train_vars[i], i, train_data[:, i], train_labels, title='train')
           
#======================================================================================================        
def PlotRNNInputs():

    if len(args) != 1:
        log.warning('Wrong number of command line arguments: %s' %len(args))
        return

    fname = args[0]

    if not os.path.isfile(fname):
        log.warning('Input file does not exist: %s' %fname)
        return    

    ifile = h5py.File(fname, 'r')

    log.info('%s contains %d dataset keys' %(fname, len(ifile.keys())))

    maxl = len(max(ifile.keys(), key=len))
    
    for k in ifile.keys():
        d = ifile[k]        
        log.info('   %s len=%6d, size=%6d, dtype=%8s, shape=%s' %(('%s' %k).ljust(maxl), len(d), d.size, d.dtype, d.shape))

    lep_train_vars   = ifile['lep_train_vars'][:]
    jet_train_vars   = ifile['jet_train_vars'][:]
    extra_vars       = ifile['extra_vars'][:]

    test_lep_data   = ifile['test_lep_data'][:]
    test_jet_data   = ifile['test_jet_data'][:]

    test_labels     = ifile['test_labels'][:]
    
    test_extra_data = ifile['test_extra_data'][:]
    test_met_data   = GetMETVector(test_extra_data,  extra_vars)
    
    if options.do_rotation:
        #for i in range(3):
        #    RotationData(test_lep_data[i], test_lep_data[i][0])

        sub_leps = Parallel(n_jobs=40)(delayed(RotationData)(test_lep_data[i], test_lep_data[i][0]) for i in range(len(test_lep_data)))
        sub_jets = Parallel(n_jobs=40)(delayed(RotationData)(test_jet_data[i], test_lep_data[i][0]) for i in range(len(test_jet_data)))
        sub_mets = Parallel(n_jobs=40)(delayed(RotationData)(test_met_data[i], test_lep_data[i][0]) for i in range(len(test_met_data)))
        
        test_lep_data = np.concatenate(sub_leps, axis=0).reshape(test_lep_data.shape)
        test_jet_data = np.concatenate(sub_jets, axis=0).reshape(test_jet_data.shape)
        test_met_data = np.concatenate(sub_mets, axis=0).reshape(test_met_data.shape)

    lep_data_bkg = test_lep_data[test_labels == 0].reshape(-1, test_lep_data.shape[2])
    lep_data_sig = test_lep_data[test_labels == 1].reshape(-1, test_lep_data.shape[2])

    log.info('test lepton WH data shape = {}'.format(lep_data_sig.shape))
    log.info('test lepton WZ data shape = {}'.format(lep_data_bkg.shape))
    
    jet_data_bkg = test_jet_data[test_labels == 0].reshape(-1, test_jet_data.shape[2])
    jet_data_sig = test_jet_data[test_labels == 1].reshape(-1, test_jet_data.shape[2])

    met_data_bkg = test_met_data[test_labels == 0].reshape(-1, test_met_data.shape[2])
    met_data_sig = test_met_data[test_labels == 1].reshape(-1, test_met_data.shape[2])


    log.info('Make plots...')
           
    for i in range(len(lep_train_vars)):
        plotRNNVarData(lep_train_vars[i], i, lep_data_sig[:, i], lep_data_bkg[:, i])

    for i in range(len(jet_train_vars)):
        plotRNNVarData(jet_train_vars[i], i, jet_data_sig[jet_data_sig[:, 2] > 100][:, i], jet_data_bkg[jet_data_bkg[:, 2] > 100][:, i])
      
    met_train_vars = ['m_met_Eta', 'm_met_Phi', 'm_met_MET']

    for i in range(len(met_train_vars)):
        plotRNNVarData(met_train_vars[i], i, met_data_sig[:, i], met_data_bkg[:, i])
      

#======================================================================================================        
if __name__ == '__main__':

    timeStart = time.time()

    log.info('Start job at %s:%s' %(socket.gethostname(), os.getcwd()))
    log.info('Current time: %s' %(time.asctime(time.localtime())))
   
    if options.do_rnn:
        PlotRNNInputs()
    else:
        main()

    log.info('Local time: %s' %(time.asctime(time.localtime())))
    log.info('Total time: %.1fs' %(time.time()-timeStart))
