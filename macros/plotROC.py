#!/usr/bin/env python

import logging
import math
import os
import socket
import sys
import time

import h5py
import numpy as np
import keras

import matplotlib

from keras.models import Sequential
from keras.layers import Dense, Dropout

from optparse import OptionParser
p = OptionParser()

p.add_option('--outdir', '-o',   type='string',        default=None)
p.add_option('--key',    '-k',   type='string',        default=None)
p.add_option('--min-nevent',     type='int',           default=400)
p.add_option('--nclass',         type='int',           default=3)

p.add_option('--debug', '-d',    action='store_true',  default=False)
p.add_option('--batch', '-b',    action='store_true',  default=False)
p.add_option('--show',  '-s',    action='store_true',  default=False)
p.add_option('--show-extra',     action='store_true',  default=False)
p.add_option('--predict',        action='store_true',  default=False)

p.add_option('--do-all-rocs',    action='store_true',  default=False)
p.add_option('--no-roc',         action='store_true',  default=False)
p.add_option('--no-acc',         action='store_true',  default=False)
p.add_option('--no-mlp',         action='store_true',  default=False)

(options,args) = p.parse_args()

if options.batch and not options.show:
    matplotlib.use('Agg')

import matplotlib.pyplot as plt # import pyplot AFTER setting batch backend

#======================================================================================================
def getLog(name, level='INFO', debug=False, print_time=False):

    if print_time:
        f = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
    else:
        f = logging.Formatter('%(message)s')
        
    sh = logging.StreamHandler()
    sh.setFormatter(f)

    logging._warn_preinit_stderr = 0
    
    log = logging.getLogger(name)

    log.handlers  = []
    log.propagate = False
    
    log.addHandler(sh)
    
    if debug:
        log.setLevel(logging.DEBUG)
    else:
        if level == 'DEBUG':   log.setLevel(logging.DEBUG)
        if level == 'INFO':    log.setLevel(logging.INFO)
        if level == 'WARNING': log.setLevel(logging.WARNING)    
        if level == 'ERROR':   log.setLevel(logging.ERROR)

    return log

#======================================================================================================
log = getLog(os.path.basename(__file__), print_time=False)

#======================================================================================================
class ClassData:

    def __init__(self, class_name, class_label, class_extra_data, class_weights):

        self.class_name       = class_name
        self.class_label      = class_label
        self.class_extra_data = class_extra_data
        self.class_weights    = class_weights
        self.input_data       = None
        
#======================================================================================================
class EffData:

    def __init__(self, class_name, class_label, var_name, var_label):

        self.class_name  = class_name
        self.class_label = class_label

        self.var_name    = var_name
        self.var_label   = var_label

    def CompEff(self, data_num, data_den, bins):

        bins = sorted(bins)
    
        count_den   = np.zeros(len(bins))
        count_num   = np.zeros(len(bins))
    
        self.xval = np.zeros(len(bins))
        self.xerr = np.zeros(len(bins)) 
    
        for i in range(len(bins)):
            bin_curr = bins[i]
        
            if i+1 < len(bins):
                bin_next = bins[i+1]

                self.xval[i] = 0.5*(bin_curr + bin_next)
                self.xerr[i]  = 0.5*(bin_next - bin_curr)
            
                n0 = data_num >= bin_curr
                n1 = data_num <  bin_next
                na = np.logical_and(n0, n1)
                
                d0 = data_den >= bin_curr
                d1 = data_den <  bin_next
                da = np.logical_and(d0, d1)
                
                count_num[i] = np.sum(na)
                count_den[i] = np.sum(da)      
                
            else:
                bin_prev = bins[i-1]

                self.xerr[i]  = 0.5*(bin_curr - bin_prev)
                self.xval[i] = bin_curr + self.xerr[i]
            
                count_num[i] = np.sum(data_num >= bin_curr)
                count_den[i] = np.sum(data_den >= bin_curr)

        self.yval = np.zeros(len(bins))
        self.yerr = np.zeros(len(bins))

        for i in range(len(bins)):
            n = count_num[i]
            d = count_den[i]

            if d > 0 and n > 0:
                self.yval[i] = n/d
                self.yerr[i] = (n/d)/math.sqrt(n)

#======================================================================================================
def saveFig(plt, name):
    
    outname = getOutName(name)

    if not outname:
        return

    log.info('saveFig - %s' %outname)
    
    plt.savefig(outname, format='pdf')
    
#======================================================================================================
def getOutName(name):
    
    if not options.outdir:
        return None
        
    outdir = '%s' %(options.outdir.rstrip('/'))
        
    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    return '%s/%s' %(outdir.rstrip('/'), name)

#======================================================================================================
def getClassColor(class_name, isTrain=False):

    colors = {'WH': 'blue',
              'WZ': 'red'}

    colors_train = {'WH': 'lightblue',
                    'WZ': 'tomato'}

    if isTrain:
        return colors_train[class_name]

    return colors[class_name]

#======================================================================================================
def getClassAsLatex(class_name):

    colors = {'WH': '\mathrm{WH}',
              'WZ': '\mathrm{WZ}'}

    
    try:
        return colors[class_name]
    except:
        raise Exception('getClassAsLatex - unknown class_name="%s"' %class_name)

        
#======================================================================================================
def plotROC(data_sig, data_bkg, name_sig, name_bkg, outname, use_greater=True, min_sigeff=None):

    '''Plot data for selected variable'''

    
    sig_effs = []
    bkg_effs = []
    bkg_rejs = []
    cut_vals = []

    sig_total = float(len(data_sig))
    bkg_total = float(len(data_bkg))

    log.info('===============================================================================')
    log.info('plotROC - %s' %outname)

    log.info('Sig: %-12s size=%6d' %(name_sig, len(data_sig)))
    log.info('Bkg: %-12s size=%6d' %(name_bkg, len(data_bkg)))

    sig_benchs = sorted([80, 85, 90, 95], reverse=(not use_greater))
    sig_result = {}
    
    for i in range(1, 1000):
        cutv = 1.0 - float(i)/1000.0

        if use_greater:
            select_sig = data_sig > cutv
            select_bkg = data_bkg > cutv
        else:
            select_sig = data_sig < cutv
            select_bkg = data_bkg < cutv            
        
        sig_npass = np.sum(select_sig)
        bkg_npass = np.sum(select_bkg)

        sig_eff = float(sig_npass)/sig_total
        bkg_eff = float(bkg_npass)/bkg_total

        if not bkg_eff > 0.0:
            continue

        if min_sigeff and sig_eff < min_sigeff:
            continue

        if sig_npass < options.min_nevent or bkg_npass < options.min_nevent:
            continue

        bkg_rej = 1.0/bkg_eff

        sig_effs += [sig_eff]
        bkg_effs += [bkg_eff]
        bkg_rejs += [bkg_rej]
        cut_vals += [cutv]

        for sig_bench in sig_benchs:
            if (use_greater and sig_eff > 0.01*sig_bench) or (not use_greater and sig_eff < 0.01*sig_bench):
                
                if use_greater:
                    msg = 'RNN > %.4f' %cutv
                else:
                    msg = 'RNN < %.4f' %cutv

                msg += ', sig_eff=%.4f, bkg_eff=%.4f, bkg_rej=%6s' %(sig_eff, bkg_eff, '%.1f' %bkg_rej)
                    
                log.info('%s, nsig=%8d, nbkg=%7d' %(msg, sig_npass, bkg_npass))

                sig_result[sig_bench] = (cutv, sig_eff, bkg_eff)
                
                sig_benchs.remove(sig_bench)
                break

    plt.clf()
    plt.plot(sig_effs, bkg_rejs)

    plt.xlabel('$\epsilon^{%s}$'   %getClassAsLatex(name_sig))
    plt.ylabel('1/$\epsilon^{%s}$' %getClassAsLatex(name_bkg))
    plt.grid(True)

    saveFig(plt, '%s.pdf' %(outname))

    plt.yscale('log')
    saveFig(plt, '%s_logy.pdf' %(outname))

    if options.show:
        plt.show()

    return (sig_result, sig_effs, bkg_rejs)


#======================================================================================================
def plot1ROCs(class_predictions):

    w0 = class_predictions[1].class_weights
    w1 = class_predictions[0].class_weights

    n0 = class_predictions[1].class_name
    n1 = class_predictions[0].class_name
 
    wh_vs_wz    = plotROC(w0[:,0], w1[:,0], n0, n1, 'ROC_%s_RNN_%s_rej_%s' %(n0, n0, n1))

    if options.do_all_rocs:
        plotROC(w0[:,0], w1[:,0], n0, n1, 'ROC_%s_RNN_%s_rej_%s_zoom' %(n0, n0, n1), min_sigeff=0.7)

    return wh_vs_wz
    
#======================================================================================================
def plotWeights(class_predictions, class_labels, select_class, outname, xtitle, logy=False):

    '''Plot data for selected RNN class'''

    #
    # Plot 1d histogram with variable values
    #
    plt.clf()
    
    for iclass, class_data in class_predictions.items():
        cname = class_labels[iclass]
        cdata = class_data.class_weights[:,select_class]
        
        color = getClassColor(cname)
        label = '$%s$' %getClassAsLatex(cname)

        log.info('plotWeights - data len=%8d, class %s with color="%s"' %(len(cdata), cname, color))

        pd = plt.hist(cdata, bins=100, histtype='step', color=color, label=label, log=logy, density=True)

    plt.autoscale(enable=True, axis='both', tight=None)
    
    plt.legend(loc='upper center')
    plt.xlabel(xtitle)
    plt.ylabel('Entries')
    plt.title(xtitle)
    plt.grid(True)
               
    saveFig(plt, '%s.pdf' %(outname))

    if options.show:
        plt.show()
    
#======================================================================================================
def plotCompareWeights(train_class_predictions, test_class_predictions, class_labels, select_class, outname, xtitle, logy=False):

    '''Plot data for selected RNN class'''

    #
    # Plot 1d histogram with variable values
    #
    plt.clf()
    
    for iclass, class_data in train_class_predictions.items():
        cname = class_labels[iclass]
        cdata = class_data.class_weights[:,select_class]
        
        color = getClassColor(cname, True)
        label = '$%s train$' %getClassAsLatex(cname)

        log.info('plotWeights - data len=%8d, class %s with color="%s"' %(len(cdata), cname, color))

        pd = plt.hist(cdata, bins=100, histtype='stepfilled', color=color, label=label, log=logy, density=True, alpha= 0.5)

    for iclass, class_data in test_class_predictions.items():
        cname = class_labels[iclass]
        cdata = class_data.class_weights[:,select_class]
        
        color = getClassColor(cname)
        label = '$%s test$' %getClassAsLatex(cname)

        log.info('plotWeights - data len=%8d, class %s with color="%s"' %(len(cdata), cname, color))

        pd = plt.hist(cdata, bins=100, histtype='step', color=color, label=label, log=logy, density=True)


    plt.autoscale(enable=True, axis='both', tight=None)
    
    plt.legend(loc='upper center')
    plt.xlabel(xtitle)
    plt.ylabel('Entries')
    plt.title(xtitle)
    plt.grid(True)
               
    saveFig(plt, '%s.pdf' %(outname))

    if options.show:
        plt.show()


#======================================================================================================
def plotAccLoss():

    '''Plot accuracy and loss as function of epoch'''

    #
    # Plot 1d histogram with variable values
    #
    fpath_loss = '%s_loss.csv' %(options.key)

    if not os.path.isfile(fpath_loss):
        raise Exception('plotAccLoss - CSVLogger file does not exist: %s' %fpath_loss)

    ifile = np.genfromtxt(fpath_loss, dtype=float, delimiter=',', names=True)
               
    plt.clf()
    plt.plot(ifile['acc'])
    plt.plot(ifile['val_acc'])
    plt.title('model accuracy')
    plt.ylabel('accuracy')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left') 

    saveFig(plt, '%s.pdf' %('overtraining_acc_epoch'))

    plt.clf()
    plt.plot(ifile['loss'])
    plt.plot(ifile['val_loss'])
    plt.title('model loss')
    plt.ylabel('loss')
    plt.xlabel('epoch')
    plt.legend(['train', 'test'], loc='upper left') 
    
    saveFig(plt, '%s.pdf' %('overtraining_loss_epoch'))

    if options.show:
        plt.show()

#======================================================================================================
def makeClassData(mlp_predictions, input_labels, class_labels):

    '''Plot RNN score'''
    
    log.info('makeClassData - moded was trained with %d classes: '%len(class_labels))
    
    for iclass in range(len(class_labels)):
        log.info('  class %d - %s' %(iclass, class_labels[iclass]))

    log.info('input_labels     len=%s, shape=%s, dtype=%s' %(len(input_labels),     input_labels    .shape, input_labels    .dtype))
    log.info('mlp_predictions  len=%s, shape=%s, dtype=%s' %(len(mlp_predictions),  mlp_predictions .shape, mlp_predictions .dtype))
    
    #
    # Evaluate MLP for events in each class
    #
    class_data_list = {}

    for iclass in range(len(class_labels)):
        log.info('Read pre-computed model predictions for class %d = %s with %d events' %(iclass, class_labels[iclass], len(input_labels)))
                        
        class_weights = mlp_predictions[input_labels == iclass]    

        class_data_list[iclass] = ClassData(class_labels[iclass], iclass, None, class_weights)

    return class_data_list


#======================================================================================================
def loadModel():

    log.info('Will load model...')

    fpath_arch   = '%s_arch.json'      %(options.key)
    fpath_weight = '%s_weights.h5'     %(options.key)
    fpath_pred   = '%s_predictions.h5' %(options.key)
    
    if not os.path.isfile(fpath_arch):
        raise Exception('loadModel - architechture file does not exist: %s' %fpath_arch)

    if not os.path.isfile(fpath_weight):
        raise Exception('loadModel - weight file does not exist: %s' %fpath_weight)

    if not os.path.isfile(fpath_pred):
        raise Exception('loadModel - predictions file does not exist: %s' %fpath_pred)
    
    with open(fpath_arch, 'r') as model_file:
        model = keras.models.model_from_json(model_file.read())

    model.load_weights(fpath_weight)

    log.info('Loaded weights from: %s' %fpath_weight)
        
    pfile = h5py.File(fpath_pred, 'r')
    
    train_data_predictions = pfile['train_data_predictions'][:]
    test_data_predictions  = pfile['test_data_predictions'][:]

    return (model, train_data_predictions, test_data_predictions)

#======================================================================================================
def main():

    if len(args) != 1:
        log.warning('Wrong number of command line arguments: %s' %len(args))
        return

    fname = args[0]

    if not os.path.isfile(fname):
        log.warning('Input file does not exist: %s' %fname)
        return    

    ifile = h5py.File(fname, 'r')

    log.info('%s contains %d dataset keys' %(fname, len(ifile.keys())))

    maxl = len(max(ifile.keys(), key=len))
    
    for k in sorted(ifile.keys()):
        d = ifile[k]        
        log.info('   %s len=%8d, size=%12d, dtype=%8s, shape=%s' %(('%s' %k).ljust(maxl), len(d), d.size, d.dtype, d.shape))

    train_labels = ifile['train_labels'][:]
    test_labels  = ifile['test_labels'][:]
    
    if False:
        #
        # Do not load by default data used for RNN training - takes lots of memory
        #
        test_data  = ifile['test_data'][:]        
        train_data = ifile['train_data'][:]

        if len(train_data) != len(train_labels):
            raise Exception('Length of test data and labels do not match')

        if len(test_data) != len(test_labels):
            raise Exception('Length of test data and labels do not match')    

    model, train_mlp_predictions, test_mlp_predictions = loadModel()

    # save the diagram of the model
    keras.utils.plot_model(model, to_file=getOutName('model.pdf'), show_shapes=True)

    model.summary()

    log.info('MLP predictions for train events: size=%d, shape=%s' %(len(train_mlp_predictions), str(train_mlp_predictions.shape)))
    log.info('MLP predictions for test  events: size=%d, shape=%s' %(len(test_mlp_predictions),  str(test_mlp_predictions.shape)))

    #----------------------------------------------------------------------------------------
    # Create ClassData objects
    #   
    class_labels = ['WZ', 'WH'] # relate to where WZ=0, WH=1 in mlp output

    train_class_data = makeClassData(train_mlp_predictions, train_labels, class_labels)
    test_class_data  = makeClassData(test_mlp_predictions,  test_labels,  class_labels)

    #----------------------------------------------------------------------------------------
    # Plot RNN weights
    #
    if not options.no_mlp:
        for data in test_class_data.values():
            xtitle  = 'MLP weights for $%s$' %getClassAsLatex(data.class_name)
            outname = 'MLP_weights_%s' %data.class_name
            
            plotWeights(test_class_data, class_labels, 0, outname,        xtitle)
            plotWeights(test_class_data, class_labels, 0, outname+'_log', xtitle, logy=True)
   
            plotCompareWeights(train_class_data, test_class_data, class_labels, 0, outname+'_comp',        xtitle)
            plotCompareWeights(train_class_data, test_class_data, class_labels, 0, outname+'_comp'+'_log', xtitle, logy=True)
            

    #----------------------------------------------------------------------------------------
    # Plot ROC curves
    #
    if not options.no_roc:
        eff_cuts = plot1ROCs(test_class_data)
        
    #----------------------------------------------------------------------------------------
    # Plot accuracy and loss as function of epoch
    #
    if not options.no_acc:
        plotAccLoss()      
        

#======================================================================================================
if __name__ == '__main__':

    timeStart = time.time()

    log.info('Start job at %s:%s' %(socket.gethostname(), os.getcwd()))
    log.info('Current time: %s' %(time.asctime(time.localtime())))

    main()

    log.info('Local time: %s' %(time.asctime(time.localtime())))
    log.info('Total time: %.1fs' %(time.time()-timeStart))
