#!/usr/bin/env python

import os
import socket
import sys
import time
import logging

import h5py
import numpy as np
import pandas as pd

import matplotlib
import matplotlib.pyplot as plt
import keras

from keras.models import Sequential
from keras.layers import Dense, Dropout
from sklearn import preprocessing

from optparse import OptionParser
p = OptionParser()

p.add_option('-d', '--debug',    action='store_true', default=False)

p.add_option('-n', '--nevent',   type='int',    default=None)
p.add_option('-o', '--outdir',   type='string', default='models/')
p.add_option('--outkey',         type='string', default='mlp')
p.add_option('--scaler',         type='string', default=None)

(options,args) = p.parse_args()

#======================================================================================================        
def getLog(name, level='INFO', debug=False, print_time=False):

    if print_time:
        f = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
    else:
        f = logging.Formatter('%(message)s')
        
    sh = logging.StreamHandler()
    sh.setFormatter(f)

    logging._warn_preinit_stderr = 0
    
    log = logging.getLogger(name)

    log.handlers  = []
    log.propagate = False
    
    log.addHandler(sh)
    
    if debug:
        log.setLevel(logging.DEBUG)
    else:
        if level == 'DEBUG':   log.setLevel(logging.DEBUG)
        if level == 'INFO':    log.setLevel(logging.INFO)
        if level == 'WARNING': log.setLevel(logging.WARNING)    
        if level == 'ERROR':   log.setLevel(logging.ERROR)

    return log

#======================================================================================================        
log = getLog(os.path.basename(__file__), print_time=False)

#======================================================================================================        
def countEvents(labels, select_label):

    if select_label == None:
        return len(labels)

    icount = 0

    for i in range(len(labels)):
        if labels[i] == select_label:
            icount += 1

    return icount


#======================================================================================================
def getOutName(name):
    
    if not options.outdir:
        return None
    
    outdir = '%s/' %(options.outdir.rstrip('/'))
    
    if not os.path.isdir(outdir):
        os.makedirs(outdir)

    return '%s/%s' %(outdir.rstrip('/'), name)


#======================================================================================================        
def saveModel(model, train_data=[], test_data=[], outkey='mlp'):

    if options.outkey:
        outkey = getOutName(options.outkey)
    else:
        outkey = getOutName(outkey)


    fname_model  = '%s_model.h5'       %outkey
    fname_weight = '%s_weights.h5'     %outkey
    fname_arch   = '%s_arch.json'      %outkey
    fname_pred   = '%s_predictions.h5' %outkey

    if model == None:
        #   
        # Just check that output files do not already exist
        #   
        for f in [fname_model, fname_weight, fname_arch, fname_pred]:
            if os.path.isfile(f):
                raise Exception('saveModel - file already exists: \"%s\"' %f) 

        return 
    
    #   
    # Save model, weights and architecture as json
    #   
    model.save_weights(fname_weight)
    model.save        (fname_model)
    
    with open(fname_arch, 'w') as arch:
        arch.write(model.to_json(indent=2))    

    log.info('saveModel - saved model to:        %s' %fname_model)
    log.info('saveModel - saved weights to:      %s' %fname_weight)
    log.info('saveModel - saved architecture to: %s' %fname_arch)

    log.info('saveModel - save model predictions for test and training data')
    
    pfile = h5py.File(fname_pred, 'w')

    if len(train_data):
        pfile.create_dataset('train_data_predictions', data=model.predict(train_data))
        print(model.predict(train_data))

    if len(test_data):
        pfile.create_dataset('test_data_predictions',  data=model.predict(test_data))
        print(model.predict(test_data)) 

    log.info('saveModel - all done')


#======================================================================================================        
def trainMLP(train_data, train_labels, train_weights, test_data, test_labels, title):

    '''Train simple MLP algorithm'''
    timeStart = time.time()
 
    log.info('trainMLP - with title=%s' %(title))
    log.info('   train_data   len=%s, shape=%s, dtype=%s' %(len(train_data),   train_data  .shape, train_data  .dtype))
    log.info('   train_labels len=%s, shape=%s, dtype=%s' %(len(train_labels), train_labels.shape, train_labels.dtype))    

    log.info('   test_data   len=%s, shape=%s, dtype=%s' %(len(test_data),   test_data  .shape, test_data  .dtype))
    log.info('   test_labels len=%s, shape=%s, dtype=%s' %(len(test_labels), test_labels.shape, test_labels.dtype)) 

    log.info('Will configure model...')
   
    ndim = train_data.shape[1]

    model = Sequential()
    model.add(Dense(64, input_dim=ndim, activation='relu'))
    model.add(Dropout(0.5))

    for i in range(2):
        model.add(Dense(200, activation='relu'))
        #model.add(Dense(300, activation='relu', kernel_initializer='random_uniform'))
        model.add(Dropout(0.1))
 
    model.add(Dense(1, activation='sigmoid'))

    log.info('Will compile model...')
    
    model.compile(loss='binary_crossentropy',
                  optimizer='adam',
                  metrics=['accuracy'])

    log.info('Will fit model...')
    
    csv_logger = keras.callbacks.CSVLogger(getOutName('%s_loss.csv'%options.outkey))

    model.fit(train_data, train_labels,
              epochs=40,
              batch_size=1024,
              shuffle=True,
              validation_data=(test_data, test_labels),
              #class_weight={0:1.0, 1:53.6}, 
              sample_weight=train_weights,
              callbacks=[csv_logger])

    log.info('trainMLP - all done in %.1f seconds' %(time.time()-timeStart))
    log.info('Will evaluate model...')
   
    model.summary()

    score = model.evaluate(test_data, test_labels, batch_size=128)

    log.info('Training score: %s' %(score))

    return model


#======================================================================================================        
def main():

    if len(args) != 1:
        log.warning('Wrong number of command line arguments: %s' %len(args))
        return

    fname = args[0]

    if not os.path.isfile(fname):
        log.warning('Input file does not exist: %s' %fname)
        return    

    ifile = h5py.File(fname, 'r')

    log.info('%s contains %d dataset keys' %(fname, len(ifile.keys())))

    maxl = len(max(ifile.keys(), key=len))
    
    for k in ifile.keys():
        d = ifile[k]        
        log.info('   %s len=%6d, size=%6d, dtype=%8s, shape=%s' %(('%s' %k).ljust(maxl), len(d), d.size, d.dtype, d.shape))

    train_vars = ifile['train_vars'][:]
    log.info('Available variables in the input file are: %s'%(', '.join(train_vars)))

    train_data   = ifile['train_data'][:]   
    train_labels = ifile['train_labels'][:]

    test_data    = ifile['test_data'][:]
    test_labels  = ifile['test_labels'][:]
    
    if len(test_data) != len(test_labels):
        raise Exception('Length of test data and labels do not match')

    if len(train_data) != len(train_labels):
        raise Exception('Length of test data and labels do not match')
    
    log.info('Select indices for test events...')
    
    vars_set = ['DRll', 'METFinalEMTopo_MET', 'Mll', 'NJet', 'RankMJetsLep1_DPhiJetsLep', 'RankMJetsLep1_DRJetsLep', 'RankMJetsLep1_MJetsLep']

    #
    # Prepare DataFrame from hdf5
    #
    train_data_df = pd.DataFrame(train_data, columns = train_vars)

    train_data_df['label']   = train_labels
    train_data_df['istrain'] = 1

    nsig = (train_labels == 1).sum()
    nbkg = (train_labels == 0).sum()
    
    train_data_df['weight'] = train_labels*(nbkg/float(nsig) - 1) + 1

    #train_data_df = train_data_df.sample(frac=1)

    log.info('N selected training signal = %d, N selected training background = %d'%(nsig, nbkg))

    # Prepare test data set
    test_data_df = pd.DataFrame(test_data, columns = train_vars)

    test_data_df['label']   = test_labels
    test_data_df['istrain'] = 0
    test_data_df['weight']  = 1

    nsig = (test_labels == 1).sum()
    nbkg = (test_labels == 0).sum()

    log.info('N selected testing signal = %d, N selected testing background = %d'%(nsig, nbkg))

    # Concat DataFrame and normalize inputs
    all_data_df = pd.concat([train_data_df, test_data_df], axis = 0, ignore_index = True)
    all_inputs  = all_data_df[vars_set].values
    
    if options.scaler:
        if options.scaler == 'Robust':
            scaler = preprocessing.RobustScaler().fit(all_inputs)
        elif options.scaler == 'Minmax':
            scaler = preprocessing.MinMaxScaler().fit(all_inputs)
        else:
            scaler = preprocessing.StandardScaler().fit(all_inputs)
    
        train_data_set = scaler.transform(all_data_df[all_data_df['istrain'] == 1][vars_set].values) 
        test_data_set  = scaler.transform(all_data_df[all_data_df['istrain'] == 0][vars_set].values)
    
    else:
        train_data_set = all_data_df[all_data_df['istrain'] == 1][vars_set].values 
        test_data_set  = all_data_df[all_data_df['istrain'] == 0][vars_set].values 

    model = trainMLP(train_data_set, train_data_df['label'].values, train_data_df['weight'].values, test_data_set, test_labels, 'MLP')    
        
    saveModel(model, train_data_set, test_data_set)

    print(train_data_set)

#======================================================================================================        
if __name__ == '__main__':

    timeStart = time.time()

    log.info('Start job at %s:%s' %(socket.gethostname(), os.getcwd()))
    log.info('Current time: %s' %(time.asctime(time.localtime())))
    
    main()

    log.info('Local time: %s' %(time.asctime(time.localtime())))
    log.info('Total time: %.1fs' %(time.time()-timeStart))
