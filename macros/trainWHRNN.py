#!/usr/bin/env python

import os
import socket
import sys
import time
import logging

import h5py
import numpy as np
import pandas as pd
import keras

from joblib import Parallel,delayed

from keras.models import Sequential
from keras.layers import Dense, Dropout, LSTM
from sklearn import preprocessing 

from optparse import OptionParser
p = OptionParser()

p.add_option('-o', '--outdir',   type='string',        default='models/')
p.add_option('--model',          type='string',        default=None)
p.add_option(      '--scaler',   type='string',        default=None)
p.add_option('-n', '--nepoch',   type='int',           default=10)
p.add_option(      '--ndense',   type='int',           default=10)
p.add_option(      '--nhidden',  type='int',           default=50)
p.add_option(      '--nbatch',   type='int',           default=256)
p.add_option(      '--rate',     type='float',         default=0.2)

p.add_option('-d', '--debug',       action='store_true',  default=False)
p.add_option('-w', '--use-weights', action='store_true',  default=False)
p.add_option('--plot-model',        action='store_true',  default=False)
p.add_option('--add-vars',          action='store_true',  default=False)
p.add_option('--do-extra',          action='store_true',  default=False)
p.add_option('--do-rotation',       action='store_true',  default=False)

(options,args) = p.parse_args()

#======================================================================================================        
outkey = None
log    = None

#======================================================================================================        
class LossHistory(keras.callbacks.Callback):
    def on_train_begin(self, logs={}):
        self.losses = []

    def on_batch_end(self, batch, logs={}):
        self.losses.append(logs.get('loss'))

#======================================================================================================        
def getLog(name, outkey, level='INFO', debug=False, print_time=False):

    if print_time:
        f = logging.Formatter('%(asctime)s %(name)s %(levelname)s %(message)s', datefmt='%H:%M:%S')
    else:
        f = logging.Formatter('%(message)s')
        
    sh = logging.StreamHandler()
    sh.setFormatter(f)

    logging._warn_preinit_stderr = 0
    
    log = logging.getLogger(name)

    log.handlers  = []
    log.propagate = False
    
    log.addHandler(sh)
    
    if outkey:        
        fh = logging.FileHandler('%s.log' %outkey)
        fh.setFormatter(f)
        log.addHandler(fh)

    if debug:
        log.setLevel(logging.DEBUG)
    else:
        if level == 'DEBUG':   log.setLevel(logging.DEBUG)
        if level == 'INFO':    log.setLevel(logging.INFO)
        if level == 'WARNING': log.setLevel(logging.WARNING)    
        if level == 'ERROR':   log.setLevel(logging.ERROR)

    return log

#======================================================================================================        
def getOutKey():
    global outkey
    return outkey

#======================================================================================================        
def initOutKeyLogger(fname):

    global log
    global outkey

    outdir = '%s/input_%s' %(options.outdir.rstrip('/'), os.path.basename(fname).rstrip('.h5'))

    if not os.path.isdir(outdir):        
        os.makedirs(outdir)

    outkey  = '%s/model'      %outdir
    outkey += '_ndense%d'     %options.ndense
    outkey += '_nhidden%d'    %options.nhidden
    outkey += '_nepoch%d'     %options.nepoch
    outkey += '_nbatch%d'     %options.nbatch
    outkey += '_droprate%02.f'%(options.rate*100)

    if options.use_weights:
        outkey += '_use_weights'

    log = getLog(os.path.basename(__file__), outkey=outkey, print_time=False)

    log.info('initOutKeyLogger - outdir: "%s"' %outdir)
    log.info('initOutKeyLogger - outkey: "%s"' %outkey)

    print = log

#======================================================================================================        
def saveModel(model, train_data=[], test_data=[]):

    outkey = getOutKey()

    if outkey == None:
        return

    fname_model  = '%s_model.h5'       %outkey
    fname_weight = '%s_weights.h5'     %outkey
    fname_arch   = '%s_arch.json'      %outkey
    fname_pred   = '%s_predictions.h5' %outkey

    if model == None:
        #
        # Just check that output files do not already exist
        #
        for f in [fname_model, fname_weight, fname_arch, fname_pred]:
            if os.path.isfile(f):
                raise Exception('saveModel - file already exists: \"%s\"' %f)

        return 
                
    #
    # Save model, weights and architecture as json
    #
    model.save_weights(fname_weight)
    model.save        (fname_model)
    
    with open(fname_arch, 'w') as arch:
        arch.write(model.to_json(indent=2))    

    log.info('saveModel - saved model to:        %s' %fname_model)
    log.info('saveModel - saved weights to:      %s' %fname_weight)
    log.info('saveModel - saved architecture to: %s' %fname_arch)

    log.info('saveModel - save model predictions for test and training data')
    
    pfile = h5py.File(fname_pred, 'w')

    if len(train_data):
        print('train_data_predictions')
        print(model.predict(train_data))
        pfile.create_dataset('train_data_predictions', data=model.predict(train_data))

    if len(test_data):
        print('test_data_predictions')
        print(model.predict(test_data))
        pfile.create_dataset('test_data_predictions',  data=model.predict(test_data))

    log.info('saveModel - all done')


#======================================================================================================        
def prepareExtraVars(extra_data, extra_vars):
    
    ''' prepare the extra extra variables for the training '''
    
    if extra_data.shape[1] != len(extra_vars):
        raise Exception('Wrong dimension for the Extra data and Extra vars')

    extra_data_df = pd.DataFrame(extra_data, columns = extra_vars)

    extra_var_list = getExtaTrainVarList(extra_data_df.keys().values)

    return extra_data_df[extra_var_list].values

#======================================================================================================        
def getExtaTrainVarList(extra_vars):
    
    ''' Get the extra extra variables for RNN training '''
    
    var_names = []

    var_name_cand = ['NJet',
                     'METFinalEMTopo_MET',
                    ]

    if options.add_vars:
        var_name_cand += ['DRll', 
                          'Mll', 
                          'RankMJetsLep1_DPhiJetsLep',
                          'RankMJetsLep1_DRJetsLep', 
                          'RankMJetsLep1_MJetsLep']

    for var_name in var_name_cand:
        if var_name in extra_vars:
            var_names += [var_name]
        else:
            raise Exception('Can not find the extra lepton variable: %s' %var_name)

    return var_names


#======================================================================================================        
def trainRNN(train_sets, train_labels, test_sets, test_labels, class_weight):

    '''Train RNN algorithm'''

    timeStart = time.time()

    train_data = train_sets
    test_data  = test_sets

    log.info('trainRNN - start')
    log.info('   train_data       len=%s, shape=%s, dtype=%s' %(len(train_data),       train_data      .shape, train_data      .dtype))
    log.info('   train_labels     len=%s, shape=%s, dtype=%s' %(len(train_labels),     train_labels    .shape, train_labels    .dtype))

    if len(train_data) != len(train_labels):
        raise Exception('Unexpected input length for training data')

    if len(test_data) != len(test_labels):
        raise Exception('Unexpected input length for testing data')

    log.info('Will configure model...')

    timeSteps  = train_data .shape[1] # number of leptons + jets
    nFeatures  = train_data .shape[2] # number of variables 
    
    log.info('trainRNN - number of time steps (tracks):           %d' %timeSteps)
    log.info('trainRNN - number of features   (objs variables): %d' %nFeatures)

    #-----------------------------------------------------------------------------------
    # Create and connect graphs
    # 1. Prepare LSTM layer
    lepjet_inputs = keras.layers.Input(shape=(timeSteps, nFeatures), name="lepjet_inputs")

    masked_input = keras.layers.Masking()(lepjet_inputs)

    log.info('lepjet_inputs shape=%s' %lepjet_inputs.shape)
    
    lstm_fwd  = keras.layers.LSTM(options.nhidden, return_sequences=False, name='LSTM_forward')(masked_input)
    dpt_lstm1 = keras.layers.Dropout(rate=options.rate)(lstm_fwd)
    #lstm_bwd  = keras.layers.LSTM(options.nhidden, return_sequences=False, name='LSTM_backward', go_backwards=True)(dpt_lstm1)
    
    # 2. Dense layers
    #fc1 = keras.layers.Dense(options.ndense, activation='relu')(lstm_bwd)
    fc1 = keras.layers.Dense(options.ndense, activation='relu')(dpt_lstm1)
    dpt1 = keras.layers.Dropout(rate=options.rate)(fc1)
    
    fc1 = keras.layers.Dense(options.ndense, activation='relu')(dpt1)
    dpt2 = keras.layers.Dropout(rate=options.rate)(fc1)
    
    fc2 = keras.layers.Dense(options.ndense, activation='relu')(dpt2)
    
    output = keras.layers.Dense(1, activation='sigmoid', name="signal")(fc2)

    log.info('Will create model...')
    
    model = keras.models.Model(inputs=[lepjet_inputs], outputs=output)

    log.info('Will compile model...')

    #-----------------------------------------------------------------------------------
    # Compile and fit model
    #
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['acc'])
    
    model.summary()

    #-----------------------------------------------------------------------------------
    # save the diagram of the model structure for testing if need
    #
    if options.plot_model:
        keras.utils.plot_model(model, to_file='model.png', show_shapes=True)

    history = LossHistory()

    csv_logger = keras.callbacks.CSVLogger('%s_loss.csv' %getOutKey())

    log.info('Will fit model...')

    fhist = model.fit(train_sets,
                      train_labels,
                      epochs=options.nepoch,
                      shuffle=True,
                      validation_data=(test_sets, test_labels),
                      batch_size=options.nbatch,
                      class_weight={0:1.0, 1:class_weight},
                      callbacks=[history, csv_logger])

    log.info(str(fhist))
    
    model.summary()

    score = model.evaluate(test_sets, test_labels, batch_size=512)
    log.info('Training score: %s' %(score))

    log.info('trainRNN - all done in %.1f seconds' %(time.time()-timeStart))

    return model


#======================================================================================================        
def trainSuperRNN(train_sets, train_labels, test_sets, test_labels, class_weight):

    '''Train RNN algorithm'''

    timeStart = time.time()

    train_data = train_sets[0]
    test_data  = test_sets [0]

    train_extra = train_sets[1]
    test_extra  = test_sets [1]

    print (train_extra.shape)

    log.info('trainRNN - start')
    log.info('   train_data       len=%s, shape=%s, dtype=%s' %(len(train_data),       train_data      .shape, train_data      .dtype))
    log.info('   train_extra      len=%s, shape=%s, dtype=%s' %(len(train_extra),   train_extra  .shape, train_extra   .dtype))
    log.info('   train_labels     len=%s, shape=%s, dtype=%s' %(len(train_labels),     train_labels    .shape, train_labels    .dtype))

    if len(train_data) != len(train_extra):
        raise Exception('Unexpected input length for training data')

    if len(test_data) != len(test_extra):
        raise Exception('Unexpected input length for testing data')

    log.info('Will configure model...')

    timeSteps  = train_data .shape[1] # number of leptons + jets
    nFeatures  = train_data .shape[2] # number of variables 
    nExtraVars = train_extra.shape[1] # number of variables from event
    
    log.info('trainRNN - number of time steps (tracks):           %d' %timeSteps)
    log.info('trainRNN - number of features   (objs variables): %d' %nFeatures)
    log.info('trainRNN - number of extra      (extra variables): %d' %nExtraVars)

    #-----------------------------------------------------------------------------------
    # Create and connect graphs
    # 1. Prepare LSTM layer
    lepjet_inputs = keras.layers.Input(shape=(timeSteps, nFeatures), name="lepjet_inputs")

    masked_input = keras.layers.Masking()(lepjet_inputs)

    log.info('lepjet_inputs shape=%s' %lepjet_inputs.shape)
    
    lstm_fwd  = keras.layers.LSTM(options.nhidden, return_sequences=True, name='LSTM_forward')(masked_input)
    dpt_lstm1 = keras.layers.Dropout(rate=options.rate)(lstm_fwd)
    lstm_bwd  = keras.layers.LSTM(options.nhidden, return_sequences=False, name='LSTM_backward', go_backwards=True)(dpt_lstm1)
    
    # 2. Prepare extra input layer of lepton input variables
    extra_inputs = keras.layers.Input(shape=(nExtraVars, ), name="extra_inputs")
    fc0 = keras.layers.Dense(64, activation='relu')(extra_inputs)

    # 3. Combine two layers
    mlayer = keras.layers.concatenate([lstm_bwd, fc0])
    dpt1 = keras.layers.Dropout(rate=options.rate)(mlayer)
    
    fc1 = keras.layers.Dense(options.ndense, activation='relu')(dpt1)
    dpt2 = keras.layers.Dropout(rate=options.rate)(fc1)
    
    fc2 = keras.layers.Dense(options.ndense, activation='relu')(dpt2)
    
    output = keras.layers.Dense(1, activation='sigmoid', name="signal")(fc2)

    log.info('Will create model...')
    
    model = keras.models.Model(inputs=[lepjet_inputs, extra_inputs], outputs=output)

    log.info('Will compile model...')

    #-----------------------------------------------------------------------------------
    # Compile and fit model
    #
    model.compile(loss='binary_crossentropy', optimizer='adam', metrics=['acc'])
    
    model.summary()


    #-----------------------------------------------------------------------------------
    # save the diagram of the model structure for testing if need
    #
    if options.plot_model:
        keras.utils.plot_model(model, to_file='model.png', show_shapes=True)

    history = LossHistory()

    csv_logger = keras.callbacks.CSVLogger('%s_loss.csv' %getOutKey())

    log.info('Will fit model...')

    fhist = model.fit(train_sets,
                      train_labels,
                      epochs=options.nepoch,
                      shuffle=True,
                      validation_data=(test_sets, test_labels),
                      batch_size=options.nbatch,
                      class_weight={0:1.0, 1:class_weight},
                      callbacks=[history, csv_logger])

    log.info(str(fhist))
    
    model.summary()

    score = model.evaluate(test_sets, test_labels, batch_size=512)
    log.info('Training score: %s' %(score))

    log.info('trainRNN - all done in %.1f seconds' %(time.time()-timeStart))

    return model

#======================================================================================================        
def GetMETVector(extra_data, extra_vars):
    ''' prepare the extra MET variables '''

    if extra_data.shape[1] != len(extra_vars):
        raise Exception('Wrong dimension for the Extra data and Extra vars')

    extra_data_df = pd.DataFrame(extra_data, columns = extra_vars)

    extra_data_df['METFinalEMTopo_Eta'] = 0

    #
    # save to (NEvents, NObj, NObjVars) numpy array
    # For the MET, NObj = 1
    #
    met_vector = extra_data_df[['METFinalEMTopo_Eta', 'METFinalEMTopo_Phi', 'METFinalEMTopo_MET']].values.reshape(-1, 1, 3) 

    return met_vector

#======================================================================================================        
def LocalTransform(X):
    #min_x = np.array([ -4.49993229  -3.1415925  100.36167908  -1.        ])
    #max_x = np.array([4.49996614e+00 3.14159226e+00 1.54400140e+07 1.00000000e+00])
    
    min_x = np.array([ -4.5, -3.141592, 100,        -1])
    max_x = np.array([  4.5,  3.141592, 1.544e+07, 1])
    
    new_X = (X - min_x)/(max_x - min_x)

    return new_X

#======================================================================================================        
def RotationData(data, ref_obj):
    # Eta 
    if ref_obj[0] < 0:
        data[:, 0] = - data[:, 0]

    # Phi
    phi = ref_obj[1] 
    data[:, 1] = data[:, 1] - phi

    Pi = 3.1416
    data[:, 1][data[:, 1] < -Pi] = data[:, 1][data[:, 1] < -Pi] + 2*Pi
    data[:, 1][data[:, 1] >  Pi] = data[:, 1][data[:, 1] >  Pi] - 2*Pi

    return data 

#======================================================================================================        
def LabelLepJet(lep, jet, met):
    ''' label lepton as 1, jet as 0'''
    
    new_lep = np.concatenate((lep,  np.ones ((lep.shape[0], lep.shape[1], 1))), axis = 2)
    new_jet = np.concatenate((jet,  np.zeros((jet.shape[0], jet.shape[1], 1))), axis = 2)
    new_met = np.concatenate((met, -np.ones ((met.shape[0], met.shape[1], 1))), axis = 2)

    if options.debug:
        print("lep shape = ", lep.shape)
        print("newlep shape = ", new_lep.shape)
    
    return (new_lep, new_jet, new_met)

#======================================================================================================        
def main(fname): 

    ifile = h5py.File(fname, 'r')

    log.info('Input file: %s' %(fname))
    log.info('File contains %d dataset keys' %(len(ifile.keys())))

    maxl = len(max(ifile.keys(), key=len))
    
    for k in ifile.keys():
        d = ifile[k]        
        log.info('   %s len=%6d, size=%6d, dtype=%8s, shape=%s' %(('%s' %k).ljust(maxl), len(d), d.size, d.dtype, d.shape))

    lep_train_vars   = ifile['lep_train_vars'][:]
    jet_train_vars   = ifile['jet_train_vars'][:]
    extra_vars       = ifile['extra_vars'][:]
    class_labels     = ifile['class_labels'][:]

    train_lep_data   = ifile['train_lep_data'][:]
    train_jet_data   = ifile['train_jet_data'][:]
    train_labels     = ifile['train_labels'][:]
    train_extra_data = ifile['train_extra_data'][:]

    test_lep_data   = ifile['test_lep_data'][:]
    test_jet_data   = ifile['test_jet_data'][:]
    test_labels     = ifile['test_labels'][:]
    test_weights    = ifile['test_weights'][:]
    test_extra_data = ifile['test_extra_data'][:]
    
    if len(test_lep_data) != len(test_labels):
        raise Exception('Length of test data and labels do not match')

    if len(test_lep_data) != len(test_weights):
        raise Exception('Length of test data and weights do not match')

    if len(train_lep_data) != len(train_labels):
        raise Exception('Length of test data and labels do not match')

    if len(train_lep_data.shape) != 3 or len(test_lep_data.shape) != 3:
        raise Exception('Wrong array dimension for train/test data')

    for i in range(len(lep_train_vars)):
        log.info('Training track variable %-2d: %s' %(i, lep_train_vars[i]))
        
    for i in range(len(jet_train_vars)):
        log.info('Training track variable %-2d: %s' %(i, jet_train_vars[i]))
        
    for i in range(len(extra_vars)):
        log.info('Extra lepton variable %-2d: %s' %(i, extra_vars[i]))

    for i in range(len(class_labels)):
        log.info('class:  %s' %(class_labels[i]))

    #-----------------------------------------------------------------------------------
    # Prepare extra input variables 
    #
    if options.do_extra:
        train_extra_sel_raw = prepareExtraVars(train_extra_data, extra_vars)
        test_extra_sel_raw  = prepareExtraVars(test_extra_data,  extra_vars)

        all_extra_raw = np.concatenate((train_extra_sel_raw, test_extra_sel_raw), axis = 0 )

        extra_scaler = preprocessing.MinMaxScaler().fit(all_extra_raw)

        train_extra_sel = extra_scaler.transform(train_extra_sel_raw)
        test_extra_sel  = extra_scaler.transform(test_extra_sel_raw)

    #-----------------------------------------------------------------------------------
    # Prepare lepton and jet input variables 
    #
    # 1. Get MET vector
    train_met_data = GetMETVector(train_extra_data, extra_vars)
    test_met_data  = GetMETVector(test_extra_data,  extra_vars)

    # 2. Add lepton or jet label
    labeled_train_lep, labeled_train_jet, labeled_train_met = LabelLepJet(train_lep_data, train_jet_data, train_met_data)
    labeled_test_lep,  labeled_test_jet,  labeled_test_met  = LabelLepJet(test_lep_data,  test_jet_data,  test_met_data)
    
    # 3. Add both lepton and jet into the same sequence for RNN
    if labeled_train_lep.shape[2] != labeled_train_jet.shape[2]:
        raise Exception('Inconsistent number of variables per object')
    elif labeled_train_lep.shape[2] != labeled_train_met.shape[2]:
        raise Exception('Inconsistent number of variables per object')

    train_data_raw = np.concatenate((labeled_train_lep, labeled_train_met, labeled_train_jet), axis = 1)
    test_data_raw  = np.concatenate((labeled_test_lep,  labeled_test_met,  labeled_test_jet),  axis = 1)
  
    if options.do_rotation:
        sub_trains = Parallel(n_jobs=40)(delayed(RotationData)(train_data_raw[i], train_data_raw[i][0]) for i in range(len(train_data_raw)) )
        sub_tests  = Parallel(n_jobs=40)(delayed(RotationData)(test_data_raw[i], test_data_raw[i][0]) for i in range(len(test_data_raw)) )

        train_data_raw = np.concatenate(sub_trains, axis=0).reshape(train_data_raw.shape)
        test_data_raw  = np.concatenate(sub_tests,  axis=0).reshape(test_data_raw.shape)
    
    # 4. standardizing the rnn inputs 
    nvars_per_obj  = labeled_train_lep.shape[2]
    all_data_raw   = np.concatenate((train_data_raw, test_data_raw), axis = 0)
    flat_all_data  = all_data_raw.reshape(-1, nvars_per_obj)
    
    valid_index_all = flat_all_data[:, 2] > 100

    if options.scaler:
        if options.scaler == 'Robust':
            data_scaler = preprocessing.RobustScaler().fit(flat_all_data[valid_index_all])

        elif options.scaler == 'Standard':
            data_scaler = preprocessing.StandardScaler().fit(flat_all_data[valid_index_all])

        else:
            data_scaler = preprocessing.MinMaxScaler().fit(flat_all_data[valid_index_all])
            print(data_scaler.data_min_)
            print(data_scaler.data_max_)
    else:
        data_scaler = preprocessing.MinMaxScaler().fit(flat_all_data[valid_index_all])

    flat_train_raw = train_data_raw.reshape(-1, nvars_per_obj)
    flat_test_raw  = test_data_raw .reshape(-1, nvars_per_obj)

    invalid_index_train = flat_train_raw[:, 2] < 100 # var[2] = "Pt", invalid jet will fill Pt = 0
    invalid_index_test  = flat_test_raw[:, 2]  < 100

    # 4.1 reset invalid data to zero after standardize for Mask layer in RNN
    train_scale = LocalTransform(flat_train_raw)
    test_scale  = LocalTransform(flat_test_raw )
    #train_scale = data_scaler.transform(flat_train_raw)
    #test_scale  = data_scaler.transform(flat_test_raw )

    train_scale[invalid_index_train] = 0
    test_scale [invalid_index_test]  = 0

    print(train_scale)
    
    train_data = train_scale.reshape(train_data_raw.shape)
    test_data  = test_scale .reshape(test_data_raw.shape)
    
    if options.debug:
        print(all_data_raw)
        print(all_data_raw.shape)

        print(flat_all_data)
        print(flat_all_data.shape)

        tem = flat_all_data.reshape(all_data_raw.shape)
        print(tem)
        print(tem.shape)

    #-----------------------------------------------------------------------------------
    # Prepare event weights
    #    -- keep background (label = 0) samples with weight = 1; Only scale the signal (label = 1).
    #
    nsig_train = (train_labels == 1).sum()
    nbkg_train = (train_labels == 0).sum()
    
    nbkg_over_nsig = nbkg_train/float(nsig_train)
    
    if False:
        # TODO, use sample_weight in model.fit() will only weight training data sets
        train_data_weights = train_labels*(nbkg_train/float(nsig_train) - 1) + 1 

        nsig_test = (test_labels == 1).sum()
        nbkg_test = (test_labels == 0).sum()

        test_data_weights = test_labels*(nbkg_test/float(nsig_test) - 1) + 1

    #-----------------------------------------------------------------------------------
    # Check whether output files already exist before (long) training
    #
    saveModel(None)
    
    if options.do_extra:
        #-----------------------------------------------------------------------------------
        # Train RNN
        #
        if options.model:
            model = keras.models.load_model(options.model)
        else:
            model = trainSuperRNN([train_data, train_extra_sel], train_labels, [test_data, test_extra_sel], test_labels, nbkg_over_nsig)

        #-----------------------------------------------------------------------------------
        # Save model configuration and weights
        #
        saveModel(model, [train_data, train_extra_sel], [test_data, test_extra_sel])

    else:
        #-----------------------------------------------------------------------------------
        # Train RNN
        #
        if options.model:
            model = keras.models.load_model(options.model)
        else:
            model = trainRNN(train_data, train_labels, test_data, test_labels, nbkg_over_nsig)
        
        #-----------------------------------------------------------------------------------
        # Save model configuration and weights
        #
        saveModel(model, train_data, test_data)

 
#======================================================================================================        
if __name__ == '__main__':

    timeStart = time.time()

    if len(args) != 1:
        raise Exception('Wrong number of command line arguments: %s' %len(args))

    fname = args[0]

    if not os.path.isfile(fname):
        raise Exception('Input file does not exist: %s' %fname)

    initOutKeyLogger(fname)

    log.info('Start job at %s:%s' %(socket.gethostname(), os.getcwd()))
    log.info('Current time: %s' %(time.asctime(time.localtime())))
    
    main(fname)

    log.info('Local time: %s' %(time.asctime(time.localtime())))
    log.info('Total time: %.1fs' %(time.time()-timeStart))
