# AnpWHMVA

Codes for WH same-sign 2 lepton channel MVA studies

## create ntuples with tree
You should prepare a root file with one tree inside. 

## run docker/singularity
```sh
singularity run -e docker://gitlab-registry.cern.ch/fuhe/basic-python-image:latest
# or
# singularity run -e -B /home/fuhe/testarea/AnpWHMVA:/home/fuhe/testarea/AnpWHMVA -B /net/ustc_01/users/fuhe/:/net/ustc_01/users/fuhe/  docker://gitlab-registry.cern.ch/fuhe/basic-python-image:latest
# where "-B" will make other directory, i.e /net/ustc_01/users/fuhe/, to be accessable to your docker container. 

```

## Convert root to the hdf5 file.
```sh
python3 anpwhmva/macros/createRNNDataFromNtuple.py ${path_mini_ntp} -o wh_ss_incl_large --tree-name=tree_prepCand_MVA &> log_make_hdf5 &
```

## Train the RNN
```sh
python3 anpwhmva/macros/trainWHRNN.py ${path_h5_input} -o model_basic  --nepoch=40 --nhidden=300 --ndense=200 --nbatch=1024 &> log_train_basic &
# --rate: the dropout rate for the DropOut layer
```

## make plots
```sh
# plot input variables
python3 anpwhmva/macros/plotDataset.py ${path_h5_input} --do-rnn &> log_plot_input &

# plot ROC curve
python3 /home/fuhe/testarea/AnpWHMVA/source/anpwhmva/macros/plotROC.py ${path_h5_input} -k ${path_model} -b -o plot_basic --do-all-rocs &> log_plot_basic &
# -k $model: specify the output directory of RNN training. (contains: weight, RNN structure, model, prediction, loss)
```

